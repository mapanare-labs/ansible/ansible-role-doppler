# Ansible Role: Doppler

[![pipeline status](https://gitlab.com/mapanare-labs/ansible/ansible-role-doppler/badges/main/pipeline.svg)](https://gitlab.com/mapanare-labs/ansible/ansible-role-doppler/-/commits/main)

This role installs [Doppler CLI](https://docs.doppler.com/docs/cli) application on any supported host.

## Requirements

None.

## Installing

The role can be installed by running the following command:

```bash
git clone https://gitlab.com/enmanuelmoreira/ansible-role-doppler enmanuelmoreira.doppler
```

Add the following line into your `ansible.cfg` file:

```bash
[defaults]
role_path = ../
```

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    doppler_package_name: doppler
    doppler_package_state: present

## Dependencies

None.

## Example Playbook

    - hosts: all
      become: yes
      roles:
        - role: enmanuelmoreira.doppler

## License

MIT / BSD
